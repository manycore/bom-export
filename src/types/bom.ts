type FOP = typeof Manycore.Integration.FOP;
type GetOrderAsync = FOP['getOrderAsync'];
export type OrderDetail = Awaited<ReturnType<GetOrderAsync>>;
