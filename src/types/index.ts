export type IPlank = Awaited<
  ReturnType<typeof Manycore.Integration.Bom.getPlankListAsync>
>['result'][0];
export type Profile = IPlank['finishedProfile'];
export type Hole = IPlank['holes'][0];
export type Groove = IPlank['grooves'][0];
export type Number3 = Hole['start'];
/**
 * 槽工艺类型
 */
export enum EBomPlankGrooveType {
  /**
   * 板件槽
   */
  PLANK = 'PLANK',
  /**
   * 五金槽
   */
  FITTING = 'FITTING',
  /**
   * 辅助槽
   */
  AUXILIARY = 'AUXILIARY',
  /**
   * 拉米诺槽
   */
  LAMELLO = 'LAMELLO',
  /**
   * 乐扣槽
   */
  LOCK = 'LOCK',
}
/**
 * 槽几何类型
 */
export enum EGrooveGeometryType {
  /**
   * 方槽
   */
  SQUARE = 'SQUARE',
  /**
   * 圆角方槽
   */
  ROUND_CORNER = 'ROUND_CORNER',
  /**
   * 路径槽
   */
  PATH = 'PATH',
  /**
   * 异形槽
   */
  SPECIAL_SHAPED = 'SPECIAL_SHAPED',
}
/** 订单状态枚举 */
export enum OrderState {
  /** 已认领 @deprecated */
  claimed = 2,
  /** 排产中 @deprecated */
  productionScheduling = 5,
  /** 已审核 @deprecated */
  reviewed = 6,
  /** 已退回 @deprecated */
  returned = 8,
  /** 待确定 */
  confirmPending = 18,
  /** 已确定 */
  confirmed = 0,
  /** 已下单 */
  ordered = 1,
  /** 待审核 */
  reviewPending = 17,
  /** 待付款 */
  payPending = 3,
  /** 已付款 */
  paid = 4,
  /** 待收款 */
  payCollectPending = 19,
  /** 已收款 */
  payCollected = 20,
  /** 待拆单 */
  splitOrderPending = 21,
  /** 已拆单 */
  splited = 7,
  /** 合批中 */
  merging = 29,
  /** 已合批 */
  merged = 10,
  /** 已排产 */
  productionScheduled = 30,
  /** 已备料 */
  materialPrepared = 25,
  /** 已开料 */
  openedMaterial = 11,
  /** 已封边 */
  edgeSealed = 12,
  /** 已排孔 */
  holesArranged = 13,
  /** 已分拣 */
  picked = 26,
  /** 已包装 */
  packed = 14,
  /** 已入库 */
  warehoused = 15,
  /** 已发货 */
  shipped = 16,
  /** 已完成 */
  completed = 24,
  /** 已取消 */
  cancelled = 9,
}

/* ------ */

export type Point = [number, number];
export type Point3 = [number, number, number];
export interface Rect {
  width: number;
  height: number;
}
