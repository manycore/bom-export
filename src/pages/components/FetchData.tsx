import { bomApi, EBomSource, EOrderState, IBomListParams } from '@/api';
import { Mock } from '@/mock';
import { IPlank } from '@/types';
import { dateFormat } from '@/utils';
import { Form, Modal, Select, Table } from 'antd';
import type { DefaultOptionType } from 'antd/es/select';
import type { ColumnsType } from 'antd/es/table';
import React, { useEffect, useState } from 'react';

const log = console.log.bind(null, '[FetchData]');

const sourceOptions: DefaultOptionType[] = [
  { label: '订单', value: EBomSource.ordered },
  { label: '合批', value: EBomSource.merged },
];

const orderStateOptions: DefaultOptionType[] = [
  { label: '已拆单', value: EOrderState.splited },
];

interface DataType {
  id: React.Key;
  name: string;
  date: number;
  data: IPlank[];
}

const columns: ColumnsType<DataType> = [
  {
    title: '编号',
    dataIndex: 'id',
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '日期',
    dataIndex: 'date',
    render(value, record, index) {
      return dateFormat(value);
    },
  },
];

interface Pagination {
  total: number;
  pageNum: number;
  pageSize: number;
}

interface Props {
  onConfirm(data: DataType): void;
}
const FetchData: React.FC<Props> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selected, setSelected] = useState<DataType>();
  const [data, setData] = useState<DataType[]>();
  const [source, setSource] = useState<EBomSource>();
  const [orderState, setOrderState] = useState<EOrderState>();
  const [pagination, setPagination] = useState<Partial<Pagination>>({
    pageNum: 1,
    pageSize: 10,
  });

  function handlePaginationChange(params: Partial<Pagination>) {
    setPagination({
      ...pagination,
      ...params,
    });
  }

  async function refreshData(params?: Partial<IBomListParams>) {
    const res = await fetchData({
      source,
      orderState,
      pageNum: pagination.pageNum,
      pageSize: pagination.pageSize,
      ...params,
    });
    const { data: list } = res;
    setData(list);
    return res;
  }

  useEffect(() => {
    refreshData().then((res) => {
      const { total } = res;
      setPagination({ total });
    });
  }, []);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    if (!selected) {
      alert('未选择数据');
      return;
    }
    log('onConfirm', { selected });
    props.onConfirm(selected);
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <button onClick={showModal}>获取数据</button>
      <Modal
        title="获取数据"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          onValuesChange={(_, values) => {
            // log('form change');
            // console.log({ values });
            refreshData({ pageNum: 1, ...values }).then((res) => {
              handlePaginationChange({ pageNum: 1, total: res.total });
            });
          }}
        >
          <Form.Item label="来源" name="source">
            <Select options={sourceOptions} onChange={setSource} />
          </Form.Item>
          <Form.Item label="状态" name="orderState">
            <Select
              options={orderStateOptions}
              onChange={setOrderState}
              allowClear
            />
          </Form.Item>
        </Form>
        <Table
          rowKey={'id'}
          rowSelection={{
            type: 'radio',
            onSelect(record) {
              setSelected(record);
            },
          }}
          columns={columns}
          dataSource={data}
          onChange={() => {
            // refreshData();
          }}
          pagination={{
            total: pagination.total,
            current: pagination.pageNum,
            pageSize: pagination.pageSize,
            onChange: (pageNum, pageSize) => {
              handlePaginationChange({ pageNum, pageSize });
              refreshData({ pageNum, pageSize });
            },
          }}
        />
      </Modal>
    </>
  );
};

async function fetchData(params: IBomListParams) {
  log({ params });
  const mockList = await Mock.bomApi.list();
  const res = await bomApi.list(params);
  const { data: list, hasMore } = res;
  let { total } = res;
  let data = [...list];
  if (!hasMore) {
    data = [...data, ...mockList];
    total++;
  }
  log({ data });
  return { data, total };
}

export default FetchData;
