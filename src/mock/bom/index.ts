import result from './result.json';
import { IPlank } from '@/types';

const planks: IPlank[] = result.d.result as any;

export async function getPlanks() {
  return planks;
}

export const bomApi = {
  async list() {
    const planks = await getPlanks();
    return [
      {
        id: '-1',
        name: 'mock data',
        date: 1658816645984,
        data: planks,
      },
    ];
  },
};
