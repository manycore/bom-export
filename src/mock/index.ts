import * as bom from './bom/index';

export namespace Mock {
  export const getPlanks = bom.getPlanks;
  export const bomApi = bom.bomApi;
}
